import styled from "styled-components";

export const HeaderWrapper = styled.header``;

export const TopContainer = styled.div`
  .logo-wrapper {
    p {
    }
  }

  .input-group {
    .search-bar {
      span {
      }

      .vertical-line {
      }

      input {
      }
    }

    .search-btn {
    }
  }

  .contact-container {
    .wrapper {
      .dropdown-btn {
      }

      .login-icon {
      }

      .heart-icon {
      }
    }
  }
`;
