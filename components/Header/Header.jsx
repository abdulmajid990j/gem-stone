import React from 'react';
import BottomHeader from './BottomHeader';
import {HeaderWrapper} from './Header.style';
import TopHeader from './TopHeader';

const Header = () => {
  return (
    <HeaderWrapper>
      <TopHeader />
      <BottomHeader />
    </HeaderWrapper>
  )
}

export default Header;