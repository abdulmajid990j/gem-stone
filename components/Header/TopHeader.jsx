import Image from "next/image";
import { useState } from "react";
import { TopContainer } from "./Header.style";

// react icons
import { FaSearch, FaUser, FaHeart } from "react-icons/fa";
import { FiChevronDown } from "react-icons/fi";

const TopHeader = () => {
  const [isDropdown, setIsDropDown] = useState(true);
  return (
    <TopContainer>
      <div className="logo-wrapper">
        <Image src={require("../../images/Logo.jpg")} />
        <p>SILVER JEWELRY STORE</p>
      </div>
      <div className="input-group">
        <div className="search-bar">
          <span>
            <FaSearch />
          </span>
          <div className="verticle-line"></div>
          <input type="search" name="search" placeholder="search" />
        </div>
        <button className="search-btn">Search</button>
      </div>
      <div className="contact-container">
        <div className="wrapper">
          <button className="dropdown-btn">
            USD
            <FiChevronDown />
          </button>
          <button className="login-icon">
            <FaUser />
          </button>
          <button className="heart-icon">
            <FaHeart />
          </button>
        </div>
      </div>
    </TopContainer>
  );
};

export default TopHeader;
