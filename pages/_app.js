import Head from 'next/head';
import Layout from '../components/Layout';
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
    <>
    <Head>
      <title>Gems App</title>
      <link rel="icon" href="/favicon_32x32.jpg" />
    </Head>
    <main>
      <Layout>
      <Component {...pageProps} />
      </Layout>
    </main>
    </>
  )
}

export default MyApp;
